// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('app', [
  'ionic',
  /* Modules */
  'layout',
  'app.lifting',
  /* Services */
  // General
  'app.common.gps',
  'app.common.db',
  'app.common.rest',
  'app.common.service.camelcase',
  'app.common.service.loadingscreen',
  'app.common.service.localstorage',
  'app.common.service.session',
  'app.common.service.device',
  // Lifting module
  'app.common.service.liftingsync',
  'app.common.service.liftingposition'
  /* */
])
  .constant("REST_ENDPOINT", "http://mexiled.com/mobile/api/rest/")
  .constant("REST_TIMEOUT_DEFAULT", 10000)
  .constant("REST_RETRY_DEFAULT", 3)
  .constant("REST_OAUTH_SECRET", "ABCD1234")
  .constant("ENVIRONMENT", "X") // Can be P: Production, D: Demo, X (or Any other but will throw a warning): Develop
  .run(function($ionicPlatform, $rootScope, DBService, GPS, ENVIRONMENT) {
    $ionicPlatform.ready(function () {
      if (window.cordova) {
        if (window.cordova.plugins.Keyboard && cordova.platformId === "ios") {
          // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
          // for form inputs)
          if (cordova.plugins.Keyboard.hideKeyboardAccessoryBar) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          }

          // Don't remove this line unless you know what you are doing. It stops the viewport
          // from snapping when text inputs are focused. Ionic handles this internally for
          // a much nicer keyboard experience.
          cordova.plugins.Keyboard.disableScroll(true);
        }
        cordova.getAppVersion(function (version) {
          $rootScope.appVersion = version + ENVIRONMENT;
        });
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      // Action to take when back button is pressed
      $ionicPlatform.registerBackButtonAction(function (event) {
        if ($rootScope.isFormData)
          event.preventDefault();
        else
          window.plugins.appMinimize.minimize();
      }, 100);

      // Check if there is internet connection
      if (window.Connection) {
        if (navigator.connection.type === Connection.NONE) {
          $ionicPopup.confirm({
            title: 'No Internet Connection',
            content: 'Sorry, no Internet connectivity detected. Please reconnect and try again.'
          })
            .then(function (result) {
              if (!result) {
                ionic.Platform.exitApp();
              }
            });
        }
      }

      // Create DB
      if (window.cordova) {
        $rootScope.db = sqlitePlugin.openDatabase("mexiled.db", "1.0", "Mexiled", -1);
      } else {
        $rootScope.db = window.openDatabase("mexiled.db", "1.0", "Mexiled", -1);
      }
      DBService.createTables();

      // Start GPS service
      GPS.init();
    });

    $rootScope.rest = {};
    $rootScope.rest.retryCnt = 0;
    $rootScope.$on('$stateChangeStart', function () {
      $rootScope.disableDragMenu = false;
    });
  })
  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $compileProvider, $provide) {

    $stateProvider
      .state('index', {
        url: "/index",
        cache: false,
        controller: "IndexController",
        template: '<div id="Loading"></div><div id="app-version" ng-if="!!appVersion">{{appVersion}}</div>'
      });

    $urlRouterProvider.otherwise('/index');

    // Performance
    $ionicConfigProvider.platform.android.scrolling.jsScrolling(false);
    $ionicConfigProvider.views.maxCache(0);
    $ionicConfigProvider.views.transition('none');
    $ionicConfigProvider.views.swipeBackEnabled(false);
    $compileProvider.debugInfoEnabled(false);
    $provide.decorator('$rootScope', ['$delegate', function ($delegate) {
      Object.defineProperty($delegate.constructor.prototype, '$onRootScope', {
        value: function (name, listener) {
          var unsubscribe = $delegate.$on(name, listener);
          this.$on('$destroy', unsubscribe);
          return unsubscribe;
        },
        enumerable: false
      });
      return $delegate;
    }]);
  });
