/**
 * Created by gcuellar on 8/11/17.
 */
angular.module('app.lifting')
    .controller('liftingController',
    ['$scope', '$rootScope', '$state', '$ionicPlatform', '$ionicPosition', 'loadingScreen', 'localStorage', 'liftingSync', 'liftingPosition', 'GPS',
        function ($scope, $rootScope, $state, $ionicPlatform, $ionicPosition, loadingScreen, localStorage, liftingSync, liftingPosition, GPS) {
            /* Variables */
            var cleanDataObject = {
                coords: null,

                streetName: '',
                streetType: '', // Avenida, Calle, Boulevard, Calzada
                streetNumberLanes: 1,
                streetMaterial: '', // Concreto, Asfalto
                streetRidge: false,
                streetRidgeWide: false,
                streetRidgeSize: 0.5, // {meters}

                streetLightAvailability: '', // Tresbolillo, Unilateral, Doble, None
                /** NOTA: Si no hay postes quitar la fase de registro de poste **/

                poleType: '', // Metalico, Concreto, Farol, Vacio
                poleDoubleTension: false,
                poleNumberStreetLights: 0,
                wiringMode: '', // Aereo, Subterraneo
                connectionMode: 'Circuito', // Circuito, Directo
                streetLightTechnology: '', // [options]
                streetLightApplication: '', // Luminaria, Proyector
                streetLightHeight: 0, // {meters}
                streetLightArm: 0, // {meters}

                buttonDisabled: false,
                saveSubmited: false
            };
            $scope.data = {};

            $scope.selectItems = {
                street: {
                    types: [
                        {
                            value: 'Autopista',
                            tag: 'Autopista o carretera'
                        },
                        {
                            value: 'Rapida',
                            tag: 'Vía rápida o de acceso controlado'
                        },
                        {
                            value: 'Principal',
                            tag: 'Vía principal o eje vial'
                        },
                        {
                            value: 'Primaria',
                            tag: 'Vía primaria o colectora'
                        },
                        {
                            value: 'Secundaria-A',
                            tag: 'Vía secundaria tipo A'
                        },
                        {
                            value: 'Secundaria-B',
                            tag: 'Vía secundaria tipo B'
                        },
                        {
                            value: 'Secundaria-C',
                            tag: 'Vía secundaria tipo C'
                        },
                        {
                            value: 'Andador',
                            tag: 'Andador'
                        },
                        {
                            value: 'Tunel',
                            tag: 'Túnel peatonal'
                        }
                    ],
                    material: [
                        'Concreto',
                        'Asfalto'
                    ]
                },
                pole: {
                    types: [
                        'Metalico',
                        'Concreto',
                        'Farol',
                        {
                            value: 0,
                            tag: 'Vacio'
                        }
                    ]
                },
                streetLight: {
                    avail: [
                        'Tresbolillo',
                        'Unilateral',
                        'Doble',
                        {
                            value: 'none',
                            tag: 'Sin postes'
                        }
                    ],
                    tech: [
                        'LED',
                        'VSAP',
                        'AM'
                    ],
                    appln: [
                        'Proyector',
                        'Luminaria'
                    ]
                },
                wiring: {
                    mode: [
                        'Aereo',
                        'Subterraneo'
                    ]
                },
                connect: {
                    mode: [
                        'Circuito',
                        'Directo'
                    ]
                }
            };

            function resetData() {
                $rootScope.isFormData = false;
                angular.copy(cleanDataObject, $scope.data);
            }
            resetData(); // Initialize
            /* ~ */

            /* Map */
            var mapObjs = {
                map: null,
                area: null,
                marker: {
                    validPosition: null,
                    marker: null
                }
            };
            $ionicPlatform.ready(function () {
                setTimeout(createMap, 500);
            });

            function createMap() {
                var mapDOMElement = document.getElementById('map');
                var titleElementPosition = $ionicPosition.position(angular.element(mapDOMElement.previousElementSibling));
                mapDOMElement.style.top = titleElementPosition.height + 10 + "px";
                if (typeof google !== 'undefined') {
                    // Create map
                    if (!mapObjs.map) {
                        mapObjs.map = new google.maps.Map(document.getElementById('map'), {
                            center: {
                                lat: $rootScope.gps.latitude,
                                lng: $rootScope.gps.longitude
                            },
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            styles: [
                                {
                                    "featureType": "poi",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "landscape.natural",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {"color": "#cbe6a3"}
                                    ]
                                }
                            ],
                            disableDefaultUI: true,
                            zoom: 20,
                            zoomControl: true,
                            gestureHandling: "none"
                        });
                    } else {
                        mapDOMElement.parentNode.replaceChild(mapObjs.map.getDiv(), mapDOMElement);
                        mapObjs.map.setZoom(20);
                        mapObjs.map.setCenter({lat: $rootScope.gps.latitude, lng: $rootScope.gps.longitude});
                    }

                    // Create circle for area
                    if (!mapObjs.area) {
                        mapObjs.area = new google.maps.Circle({
                            map: mapObjs.map,
                            center: mapObjs.map.getCenter(),
                            radius: 5,
                            fillColor: "#F99C25",
                            strokeColor: "#F99C25",
                            clickable: false
                        });
                    } else
                        mapObjs.area.setMap(mapObjs.map);

                    // Create marker
                    if (!mapObjs.marker.marker) {
                        mapObjs.marker.marker = new google.maps.Marker({
                            map: mapObjs.map,
                            position: mapObjs.map.getCenter(),
                            draggable: true
                        });
                    } else
                        mapObjs.marker.marker.setMap(mapObjs.map);
                    if ($scope.data.coords)
                        mapObjs.marker.marker.setPosition($scope.data.coords);
                    mapObjs.marker.validPosition = mapObjs.marker.marker.getPosition();

                    google.maps.event.addListener(mapObjs.marker.marker, 'drag', function () {
                        var distance = parseInt(GPS.calculateDistance(mapObjs.area.getCenter(), mapObjs.marker.marker.getPosition(), true, true));
                        if (distance > 5)
                            mapObjs.marker.marker.setPosition(mapObjs.marker.validPosition);
                        else
                            mapObjs.marker.validPosition = mapObjs.marker.marker.getPosition();
                    });
                }
            }
            $rootScope.$on('locationChange', function () {
                if (!!mapObjs.map) {
                    mapObjs.map.setZoom(20);
                    mapObjs.map.setCenter({lat: $rootScope.gps.latitude, lng: $rootScope.gps.longitude});

                    mapObjs.area.setCenter(mapObjs.map.getCenter());

                    mapObjs.marker.validPosition = mapObjs.map.getCenter();
                    mapObjs.marker.marker.setPosition(mapObjs.marker.validPosition);
                }
            });

            /* ~ */

            /**
             * Next/End
             * @param watchCall
             */
            $scope.next = function (watchCall) {
                watchCall = watchCall || false;
                if (watchCall || !$scope.data.buttonDisabled) {
                    if (!$scope.data.coords) {
                        if (!!mapObjs.marker.validPosition) {
                            if (!watchCall) {
                                loadingScreen.show();
                                GPS.geocoding(mapObjs.marker.validPosition)
                                    .catch(function () {
                                    })
                                    .then(function (data) {
                                        if (data.hasOwnProperty('formatted_address') && data.address_components.length > 1 && data.address_components[1].hasOwnProperty('short_name'))
                                            $scope.data.streetName = data.formatted_address.replace(" " + data.address_components[0].short_name, "") || '';

                                        var key = "streetName";
                                        var previousLifting = liftingPosition.getPrevious({[key]: $scope.data[key]});
                                        console.debug(previousLifting);
                                        if (!!previousLifting) {

                                        }

                                        $rootScope.isFormData = true;
                                        $scope.data.buttonDisabled = true;
                                        $scope.data.coords = mapObjs.marker.validPosition.toJSON();
                                        loadingScreen.hide();
                                    });
                            }
                        }
                        /* ~ */
                    }
                    // If the first step (choose position) is already registered
                    else {
                        if (!watchCall)
                            $scope.data.saveSubmited = true;
                        // If the info is complete, send to WS to save it in DB, then clean it to start over
                        //cleanDataObject.wiringMode
                        if (
                            !!$scope.data.streetType && !!$scope.data.streetMaterial && !!$scope.data.streetLightAvailability
                            && ($scope.data.streetLightAvailability == 'none' || ($scope.data.poleType !== '' && !!$scope.data.wiringMode))
                            && (
                                ($scope.data.poleType === 0 || $scope.data.poleNumberStreetLights === 0) ||
                                (!!$scope.data.streetLightApplication && $scope.data.streetLightHeight > 0 && $scope.data.streetLightArm > 0)
                            )
                        ) {
                            $scope.data.buttonDisabled = false;
                            if (!watchCall) {
                                /* Defaulting/cleaning data conditioned by selected information */
                                if (!$scope.data.streetRidge)
                                    $scope.data.streetRidgeWide = false;
                                if (!$scope.data.streetRidgeWide)
                                    $scope.data.streetRidgeSize = 0;

                                if ($scope.data.poleType === 0)
                                    $scope.data.poleNumberStreetLights = 0;
                                if ($scope.data.wiringMode == 'Subterraneo')
                                    $scope.data.connectionMode = 'Circuito';

                                if ($scope.data.poleNumberStreetLights < 1) {
                                    $scope.data.streetLightTechnology = '';
                                    $scope.data.streetLightApplication = '';
                                    $scope.data.streetLightHeight = 0;
                                    $scope.data.streetLightArm = 0;
                                }
                                /* ~ */

                                var dataPost = {};
                                // Copy the saved data in the dataPost object
                                angular.copy($scope.data, dataPost);

                                // Status to standby (default value)
                                dataPost.status = liftingPosition.STATUS.STANDBY;

                                // Get the current date of the installation
                                var d = new Date();
                                function pad(s) {
                                    return (s < 10) ? '0' + s : s;
                                }
                                dataPost.registerDate = [d.getFullYear(), pad(d.getMonth() + 1), pad(d.getDate())].join('-') + ' ' + [pad(d.getHours()), pad(d.getMinutes()), pad(d.getSeconds())].join(':');
                                // row_version to same registered_date, this would be updated when the WS sync were done
                                dataPost.rowVersion = dataPost.registerDate;

                                // Save in local database, using the DBService to know when the registers was saved instead of Install service
                                loadingScreen.show();
                                console.debug(angular.copy(dataPost));
                                /*liftingPosition.setSingle(angular.copy(dataPost), false).then(function () {
                                    liftingSync.sync();

                                    // Reset/Clean Data
                                    localStorage.clean('lifting');
                                    $state.reload();

                                    loadingScreen.hide();
                                    $ionicPopup.alert({
                                        title: 'Registrado',
                                        template: 'El levantamiento se guardo correctamente.'
                                    });
                                }).catch(function () {
                                    loadingScreen.hide();
                                    $ionicPopup.alert({
                                        title: 'Error',
                                        template: 'No fue posible guardar la instalación, vuelva a intentarlo.'
                                    });
                                });*/
                            }
                        } else
                            $scope.data.buttonDisabled = true;
                    }
                }
            };
            /* Watch variables to evaluate if next/save button must be disabled */
            $scope.$watch('data', function () {
                $scope.next(true);
            }, true);

            /* localStorage functionality so we don't lose the data if the OS kill the app */
            document.addEventListener('pause', function () {
                localStorage.set('lifting', {date: new Date().getTime(), data: $scope.data});
            }, false);
            document.addEventListener('resume', recoverLocalStorage, false);
            document.addEventListener('deviceready', recoverLocalStorage, false);
            function recoverLocalStorage() {
                var localStorageData = localStorage.get('lifting');
                if (!!localStorageData) {
                    localStorageData = JSON.parse(localStorageData);

                    if (!$rootScope.isFormData && localStorageData.date >= (new Date().getTime() - (24 * 3600000))) {
                        $rootScope.isFormData = true;
                        $scope.data = localStorageData.data;
                        if (!!mapObjs.marker.marker && !!$scope.data.coords) {
                            mapObjs.marker.marker.setPosition($scope.data.coords);
                            mapObjs.marker.validPosition = new google.maps.LatLng($scope.data.coords);
                        }
                    }

                    localStorage.clean('lifting');
                }
            }
        }
    ]
    );
