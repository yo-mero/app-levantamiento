/**
 * Created by gcuellar on 8/11/17.
 */
'use strict';

angular.module('app.lifting', [])

    .config(function ($stateProvider) {
        $stateProvider
            .state('app.lifting', {
                url: '/lifting',
                cache: false,
                views: {
                    'content@app': {
                        templateUrl: 'modules/lifting/views/index.html',
                        controller: 'liftingController'
                    }
                }
            });
    });