angular.module('app').controller('layoutController',
['$scope', '$state', '$ionicPopup', 'loadingScreen', 'liftingSync', 'localStorage',
    function ($scope, $state, $ionicPopup, loadingScreen, liftingSync, localStorage) {
        $scope.goTo = function (module) {
            loadingScreen.show();
            switch (module) {
                case 'lifting':
                    if ($state.current.name != 'app.lifting') {
                        liftingSync.sync().finally(function () {
                            loadingScreen.hide();
                            localStorage.clean('all');
                            $state.go('app.lifting', {}, {reload: true});
                        });
                    } else {
                        loadingScreen.hide();
                        $ionicPopup.confirm({
                            title: '',
                            template: /*'Ya se encuentra en el modulo de levantamiento, */'¿desea reiniciar el registro de levantamiento?',
                            cancelText: 'Cancelar',
                            okText: 'Reiniciar'
                        }).then(function (response) {
                            if (response === true) {
                                localStorage.clean('all');
                                $state.go('app.lifting', {}, {reload: true});
                            }
                        });
                    }
                    break;
                default:
                    loadingScreen.hide();
                    break;
            }
        };
    }
]);
