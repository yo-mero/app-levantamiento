'use strict';

angular.module('layout', [])
    
.config(function ($stateProvider) {
    $stateProvider
        .state('app', {
            abstract: true,
            templateUrl: 'modules/layout/views/layoutTmpl.html',
            controller: 'layoutController'
        });
});