/**
 * Created by gcuellar on 9/11/17.
 */
angular.module('app.common.service.localstorage', [])
    .service('localStorage', [function () {
        var self = {};

        var APP_STORAGE_KEY = {
            "installation": "MexiledInstallationData",
            "lifting": "MexiledLiftingData"
        }; // Keys for access data in localStorage

        self.set = function (module, data) {
            if (typeof data == 'object')
                data = JSON.stringify(data);
            if (!APP_STORAGE_KEY.hasOwnProperty(module)) {
                throw "Bad key while trying to set localStorage in service";
                return false;
            }
            window.localStorage.setItem(APP_STORAGE_KEY[module], data);
            return true;
        };
        self.get = function (module) {
            if (!APP_STORAGE_KEY.hasOwnProperty(module)) {
                throw "Bad key while trying to get localStorage in service";
                return false;
            }
            return window.localStorage.getItem(APP_STORAGE_KEY[module]);
        };
        self.clean = function (module) {
            switch (module) {
                case 'all':
                    for (var index in APP_STORAGE_KEY) {
                        window.localStorage.removeItem(APP_STORAGE_KEY[index]);
                    }
                    return true;
                    break;
                default:
                    if (APP_STORAGE_KEY.hasOwnProperty(module)) {
                        window.localStorage.removeItem(APP_STORAGE_KEY[module]);
                        return true;
                    }
                    throw "Bad key while trying to clean localStorage in service";
                    return false;
                    break;
            }
        };

        return self;
    }]);