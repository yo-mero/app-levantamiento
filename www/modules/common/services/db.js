angular.module('app.common.db', [])
    .factory('DBService', ['$rootScope', '$q', '$ionicPlatform', '$log', function ($rootScope, $q, $ionicPlatform, $log) {

        var self = {};
        var session_create_query = 'CREATE TABLE IF NOT EXISTS user_data(id integer primary key, username text(50), access_token char(40), token_type char(20), refresh_token char(40), row_version timestamp default current_timestamp)';
        var session_insert_query = 'INSERT INTO user_data(id, username, access_token, token_type, refresh_token, row_version) VALUES(?,?,?,?,?,?)';
        var session_getall_query = 'SELECT * FROM user_data WHERE 1 ORDER BY id';
        var session_update_query = 'UPDATE user_data SET username=?, access_token=?, token_type=?, refresh_token=?, row_version=? WHERE id=?';
        var session_deleteall_query = 'DELETE FROM user_data WHERE true';

        var lifting_create_query = 'CREATE TABLE IF NOT EXISTS lifting (id INTEGER KEY UNIQUE, status VARCHAR(10) DEFAULT \'PENDIENTE\', register_date DATETIME, coords TEXT NOT NULL, street_name TEXT, street_type TEXT NOT NULL, street_number_lanes INTEGER, street_material TEXT NOT NULL, street_ridge INTEGER DEFAULT 0, street_ridge_wide INTEGER DEFAULT 0, street_ridge_size REAL, street_light_availability TEXT, pole_type TEXT, pole_double_tension INTEGER DEFAULT 0, pole_number_street_lights INTEGER, wiring_mode TEXT, connection_mode TEXT, street_light_technology TEXT, street_light_application TEXT, street_light_height REAL, street_light_arm REAL, sync INTEGER DEFAULT 0, row_version TIMESTAMP DEFAULT CURRENT_TIMESTAMP)';
        var lifting_insert_query = 'INSERT INTO lifting (id, status, register_date, coords, street_name, street_type, street_number_lanes, street_material, street_ridge, street_ridge_wide, street_ridge_size, street_light_availability, pole_type, pole_double_tension, pole_number_street_lights, wiring_mode, connection_mode, street_light_technology, street_light_application, street_light_height, street_light_arm, sync, row_version) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
        var lifting_update_query = 'UPDATE lifting SET status=?, register_date=?, coords=?, street_name=?, street_type=?, street_number_lanes=?, street_material=?, street_ridge=?, street_ridge_wide=?, street_ridge_size=?, street_light_availability=?, pole_type=?, pole_double_tension=?, pole_number_street_lights=?, wiring_mode=?, connection_mode=?, street_light_technology=?, street_light_application=?, street_light_height=?, street_light_arm=?, sync=?, row_version=? WHERE id=?';
        var lifting_delete_query = 'DELETE FROM lifting WHERE id=?';
        var lifting_getall_query = 'SELECT * FROM lifting WHERE 1 ORDER BY id';
        var lifting_deleteall_query = 'DELETE FROM lifting WHERE 1';

        self.createTablesQuerys = [
            session_create_query,
            lifting_create_query
        ];
        //self.createTablesQuerys.push('INSERT INTO installation (id_move, id_pole, id_lamp, installed_date, photoBefore, photoAfter, address, addr_number, street1, street2, comments, gps_coords) VALUES (1,1,3,"2017-06-05 16:58:03","","","","","","","Hardcoded","")');

        self.createTables = function () {
            var q = $q.defer();
            try {
                angular.forEach(self.createTablesQuerys, function (query) {
                    self.query(query).then(function () {
                        $log.info('Success table creation');
                        q.resolve(true);
                    }, function (error) {
                        $log.error("Error on create table " + angular.toJson(error));
                        q.reject(error);
                    });
                });
            } catch (e) {
                q.reject(e);
            }
            return q.promise;
        };
        // Process an action at DB layer
        self.DB_action = function (rxObj, service, action) {
            return eval("self.process" + service + "(rxObj, action)");
        };

        self.processSession = function (rxObj, action) {
            switch (action) {
                case "INSERT":
                    parameters = [rxObj.id, rxObj.username, rxObj.access_token, rxObj.token_type, rxObj.refresh_token, rxObj.row_version];
                    break;
                case "UPDATE":
                    parameters = [rxObj.username, rxObj.access_token, rxObj.token_type, rxObj.refresh_token, rxObj.row_version, rxObj.id];
                    break;
                default:
                    parameters = rxObj;
            }

            var query = eval("session_" + action.toLowerCase() + "_query");

            return self.query(query, parameters);
        };

        self.processLifting = function (rxObj, action) {
            switch (action) {
                case "INSERT":
                    parameters = [rxObj.id, rxObj.status, rxObj.register_date, rxObj.coords, rxObj.street_name, rxObj.street_type, rxObj.street_number_lanes, rxObj.street_material, rxObj.street_ridge, rxObj.street_ridge_wide, rxObj.street_ridge_size, rxObj.street_light_availability, rxObj.pole_type, rxObj.pole_double_tension, rxObj.pole_number_street_lights, rxObj.wiring_mode, rxObj.connection_mode,rxObj.street_light_technology, rxObj.street_light_application, rxObj.street_light_height, rxObj.street_light_arm, rxObj.sync, rxObj.row_version]
                    break;
                case "UPDATE":
                    parameters = [rxObj.status, rxObj.register_date, rxObj.coords, rxObj.street_name, rxObj.street_type, rxObj.street_number_lanes, rxObj.street_material, rxObj.street_ridge, rxObj.street_ridge_wide, rxObj.street_ridge_size, rxObj.street_light_availability, rxObj.pole_type, rxObj.pole_double_tension, rxObj.pole_number_street_lights, rxObj.wiring_mode, rxObj.connection_mode,rxObj.street_light_technology, rxObj.street_light_application, rxObj.street_light_height, rxObj.street_light_arm, rxObj.sync, rxObj.row_version, rxObj.id]
                    break;
                case "DELETE":
                    parameters = [rxObj.id];
                    break;
                default:
                    parameters = rxObj;
            }

            var query = eval("lifting_" + action.toLowerCase() + "_query");

            return self.query(query, parameters);
        };

        //method to make querys, insert, update, delete or select
        self.query = function (query, parameters) {
            parameters = parameters || [];
            var q = $q.defer();
            try {
                $rootScope.db.transaction(function (tx) {
                    //console.debug("Executing " + query);
                    tx.executeSql(query, parameters, function (tx, res) {
                        //console.debug("Success " + angular.toJson(res));
                        q.resolve(res);
                    }, function (transaction, e) {
                        $log.error('ERROR DAO Error: ' + e.message);
                        q.reject(e);
                    });
                });
            } catch (e) {
                $log.error("Error al acceder a la Base de Datos: " + e);
                q.reject(e);
            }

            return q.promise;
        };

        //process result set
        self.getResult = function (result) {
            var res = [];
            for (var i = 0; i < result.rows.length; i++) {
                res.push(result.rows.item(i));
            }
            return res;
        };

        //process single result
        self.uniqueResult = function (result) {
            var unique = null;
            //console.debug("Resultado: " + result.rows.length);
            if (result.rows.length > 0) {
                unique = result.rows.item(0);
            }

            return unique;
        };

        return self;

    }]);
