/**
 * Created by gcuellar on 8/11/17.
 */
angular.module('app.common.service.liftingposition', [])
    .service('liftingPosition', ['$rootScope', '$q', '$interval', '$log', 'camelCase', 'RestService', 'DBService',
        function ($rootScope, $q, $interval, $log, camelCase, RestService, DBService) {
            var selfObjArray = [];
            var self = {};
            var factoryName = 'Lifting';
            self.promises = [];
            self.STATUS = {
                STANDBY: "PENDIENTE",
                SENDING: "ENVIANDO",
                ERROR: "ERROR",
                SAVED: "GUARDADO"
            };

            self.init = function (dataRowVersion) {
                var deferPromise = $q.defer();

                dataRowVersion = !!dataRowVersion ? dataRowVersion : 0;
                DBService.DB_action([], factoryName, 'GETALL').then(function (result) {
                    result = DBService.getResult(result);
                    for (var i = 0; i < result.length; i++) {
                        for (var property in result[i]) {
                            selfObjArray[i][camelCase.fromString(property)] = result[i][property];
                        }
                    }
                    var syncRequired = false;
                    var timestamp = 0;
                    var indexGreatRowVersion = -1;
                    for (var i = 0, rv = 0; i < selfObjArray.length; i++) {
                        if (Date.parse(selfObjArray[i].rowVersion) > Date.parse(dataRowVersion) || selfObjArray[i].status != self.STATUS.SAVED)
                            syncRequired = true;
                        else if (Date.parse(selfObjArray[i].rowVersion) > rv) {
                            rv = Date.parse(selfObjArray[i].rowVersion);
                            indexGreatRowVersion = i;
                        }
                    }
                    if (indexGreatRowVersion >= 0)
                        timestamp = Date.parse(selfObjArray[indexGreatRowVersion].rowVersion) / 1000;

                    if (syncRequired) {
                        self.Synchronize().then(function () {
                            RestService.sendRequest(null, 'LiftingGET', 'action=all&row_version=' + timestamp + '&local_numrows=' + selfObjArray.length + (timestamp == 0 ? '&' + Device.InfoForRequest() : '')).then(function (response) {
                                if (!!response.length)
                                    self.setAll(response, true);
                                deferPromise.resolve();
                            }, function () {
                                deferPromise.reject();
                            });
                            $log.info(factoryName + ': Se enviaron los registros en espera.');
                        }, function () {
                            if (!angular.isDefined($rootScope.trySyncrhonize))
                                $rootScope.trySyncrhonize = $interval(self.Synchronize, 60000);
                            $log.error(factoryName + 'No se pudieron enviar los registros, se reintentara en un minuto');
                            deferPromise.reject();
                        });
                    } else {
                        RestService.sendRequest(null, 'LiftingGET', 'action=all&row_version=' + timestamp + '&local_install_numrows=' + selfObjArray.length + (timestamp == 0 ? '&' + Device.InfoForRequest() : '')).then(function (response) {
                            if (!!response.length)
                                self.setAll(response, true);
                            deferPromise.resolve();
                        }, function () {
                            deferPromise.reject();
                        });
                    }
                });

                return deferPromise.promise;
            };

            self.filterStatus = function (obj) {
                return obj.status == this;
            };

            self.Synchronize = function () {
                var deferPromise = $q.defer();

                var obj = selfObjArray.filter(self.filterStatus, self.STATUS.STANDBY);
                obj = obj.concat(selfObjArray.filter(self.filterStatus, self.STATUS.ERROR));
                self.promises = [];

                if (angular.isDefined($rootScope.trySyncrhonize)) {
                    $interval.cancel($rootScope.trySyncrhonize);
                    $rootScope.trySyncrhonize = null;
                }

                try {
                    for (var i = 0; i < obj.length; i++) {
                        self.promises.push(self.saveInWS(obj[i]));
                    }
                    $q.all(self.promises).then(function () {
                        self.promises = [];
                        deferPromise.resolve();
                    }, function (error) {
                        deferPromise.reject(error);
                    });
                } catch (e) {
                    $log.error("Falló envio del levantamiento", e);
                    deferPromise.reject(e);
                }

                return deferPromise.promise;
            };

            self.saveInWS = function (obj) {
                // Save
                var deferPromise = $q.defer();

                var data = null;
                for (var property in obj) {
                    data[camelCase.toSeparate(property)] = obj[property];
                }

                data.status = self.STATUS.SENDING;
                DBService.DB_action(data, factoryName, 'UPDATE').then(function () {
                    var backupID = data.id;
                    if (!data.sync)
                        data.id = 0;
                    RestService.sendRequest(data, 'LiftingPOST', '').then(function (response) {
                        data.id = backupID;
                        data.status = self.STATUS.SAVED;
                        DBService.DB_action(data, factoryName, 'UPDATE');
                        deferPromise.resolve(response);
                    }, function (error) {
                        data.id = backupID;
                        data.status = self.STATUS.ERROR;
                        DBService.DB_action(data, factoryName, 'UPDATE');
                        switch (error.code) {
                            case "ERR409":
                                RestService.sendRequest({
                                    move: factoryName,
                                    json: angular.toJson(data),
                                    message: error.message
                                }, 'ConflictPOST', '').then(function () {
                                    data.status = self.STATUS.SAVED;
                                    DBService.DB_action(data, factoryName, 'UPDATE');
                                });
                                deferPromise.resolve('Ya existe un registro identico, data:', data);
                                break;
                            default:
                                deferPromise.reject(error);
                                break;
                        }
                    });
                }, function (error) {
                    deferPromise.reject(error);
                });

                return deferPromise.promise;
            };

            self.getAll = function () {
                // Get all
                return selfObjArray;
            };

            self.getSingle = function (elementForFilter, property) {
                property = property || 'id';
                for (var i = 0; i < selfObjArray.length; i++) {
                    if (selfObjArray[i][property] == elementForFilter)
                        return selfObjArray[i];
                }
                return null;
            };

            self.getPrevious = function (filter) {
                if (selfObjArray.length > 0) {
                    if (typeof filter !== "object") {
                        var lastID = parseInt(Math.max.apply(Math, selfObjArray.map(function (obj) {
                            return obj.id;
                        })));
                        for (var i = selfObjArray.length - 1; i >= 0; i--) {
                            if (selfObjArray[i].id == lastID)
                                return selfObjArray[i];
                        }
                        return selfObjArray[selfObjArray.length - 1];
                    } else {
                        var filterKeys = Object.keys(filter);
                        for (var i = selfObjArray.length - 1; i >= 0; i--) {
                            var equalValues = true;
                            for (var k = 0; k < filterKeys.length && equalValues; k++) {
                                equalValues = selfObjArray[i][camelCase.toSeparate(filterKeys[k])] == filter[filterKeys[k]];
                                if (equalValues && k === filterKeys.length - 1)
                                    return selfObjArray[i];
                            }
                        }
                    }
                }
                return null;
            };

            self.setAll = function (newObjArray, fromWS) {
                // Set one by one
                for (var i = 0; i < newObjArray.length; i++) {
                    self.setSingle(newObjArray[i], !!fromWS);
                }
            };

            self.setSingle = function (newObj, fromWS) {
                var deferPromise = $q.defer();

                var lifting = null;
                if (!!newObj.id)
                    lifting = self.getSingle(newObj.id);

                if (!!fromWS) {
                    newObj.status = self.STATUS.SAVED;
                    newObj.sync = 1;
                }

                if (lifting !== null) {
                    // Found, update it
                    if (!fromWS)
                        newObj.sync = lifting.sync;

                    var index = selfObjArray.indexOf(lifting);
                    var dbObj = null;
                    for (var property in selfObjArray[index]) {
                        if (!!fromWS)
                            selfObjArray[index][property] = newObj[camelCase.toSeparate(property)];
                        else
                            selfObjArray[index][property] = newObj[property];
                        dbObj[camelCase.toSeparate(property)] = selfObjArray[index][property];
                    }
                    DBService.DB_action(dbObj, factoryName, "UPDATE").then(function () {
                        deferPromise.resolve();
                    }, function () {
                        deferPromise.reject();
                    });
                } else {
                    // Not found, insert it
                    newObj.sync = +(!!newObj.sync);
                    if (!fromWS)

                    var addObj = null;
                    var dbObj = null;
                    if (!fromWS)
                        addObj = newObj;
                    for (var property in newObj) {
                        if (!!fromWS)
                            addObj[camelCase.fromString(property)] = newObj[property];
                        dbObj[camelCase.toSeparate(property)] = newObj[property];
                    }

                    selfObjArray.push(addObj);

                    DBService.DB_action(dbObj, factoryName, "INSERT").then(function () {
                        deferPromise.resolve();
                    }, function () {
                        deferPromise.reject();
                    });
                }

                return deferPromise.promise;
            };

            self.clear = function () {
                // Erase all
                selfObjArray = [];
                DBService.DB_action(newObj, factoryName, "DELETEALL");
            };

            return self;
        }
    ]);