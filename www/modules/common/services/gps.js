var app = angular.module('app.common.gps', []);

app.service('GPS', ['$rootScope', '$log', '$http', function ($rootScope, $log, $http) {

    var self = {};
    var watchID;
    var defaultLocation = {latitude: 20.6928933, longitude: -103.3835317};
    var lastSteadyPosition = {latitude: null, longitude: null};
    var accuracySteadyConstant = 30.0;
    var metersToUpdateDistance = 20.0;

    self.init = function () {
        $rootScope.gps = defaultLocation;
        $rootScope.gps.validPosition = false;
        watchID = navigator.geolocation.watchPosition(self.onSuccess, self.onError, {
            maximumAge: 3000,
            timeout: 5000,
            enableHighAccuracy: false
        });
    };

    // onSuccess Callback
    // This method accepts a Position object, which contains the
    // current GPS coordinates
    //
    self.onSuccess = function (position) {
        $rootScope.gps.latitude = position.coords.latitude;
        $rootScope.gps.longitude = position.coords.longitude;
        $rootScope.gps.accuracy = position.coords.accuracy;
        $rootScope.gps.timestamp = position.timestamp;
        $rootScope.gps.validPosition = true;

        if ((parseFloat(position.coords.accuracy) < accuracySteadyConstant)
            && (Math.abs(parseFloat(position.coords.latitude)) > 0)
            && (Math.abs(parseFloat(position.coords.longitude)) > 0)) {
            // Only calculate distance when GPS is accurate enough and position has
            // changed for more than accuracySteadyConstant or if it's the first time
            if ((lastSteadyPosition.latitude === null) || (lastSteadyPosition.longitude === null)) {
                $log.info("Saving first steady position");
                lastSteadyPosition.latitude = position.coords.latitude;
                lastSteadyPosition.longitude = position.coords.longitude;
                $rootScope.$emit('locationChange');
            }
            else {
                var distance = self.calculateDistance(lastSteadyPosition, position.coords, true);
                if (parseFloat(distance) >= parseFloat(metersToUpdateDistance)) {
                    $log.info("Distance from steady: " + distance + ". Calculating all distances...");
                    lastSteadyPosition.latitude = position.coords.latitude;
                    lastSteadyPosition.longitude = position.coords.longitude;
                    $rootScope.$emit('locationChange');
                }
            }
        }
    };

    // onError Callback receives a PositionError object
    //
    self.onError = function (error) {
        $log.warn(
            'GPS Error: \n' +
            'code: ' + error.code + '\n' +
            'message: ' + error.message + '\n'
        );
        if (window.cordova && false)
            document.location.href = 'index.html';
    };

    /**
     * Calculate distance between two points sended in latitude & longitude coordinates in kilometers or meters
     * @param coords1 <object>: {latitude <number>, longitude <number>}
     * @param coords2 <object>: {latitude <number>, longitude <number>}
     * @param inMeters <boolean>: true -> returns the distance always in meters (m), false (default) -> returns the distance in kilometers (km)
     * @param noRounded <boolean>: true -> returns the distance without rounded value, false (default) -> round the distance to return multiples of 50 or 10. This feature only works on meters
     * @returns <string>: Returns the distance in km or m, if distance < 1km returns distance in m, no matters the inMeters parameter
     */
    self.calculateDistance = function (coords1, coords2, inMeters, noRounded) {
        if (coords1.toJSON !== undefined) {
            coords1.latitude = coords1.lat();
            coords1.longitude = coords1.lng();
        }
        if (coords2.toJSON !== undefined) {
            coords2.latitude = coords2.lat();
            coords2.longitude = coords2.lng();
        }

        inMeters = !!inMeters;
        noRounded = !!noRounded;
        var R = 6371; // Radius of the earth in km
        var dLat = self.deg2rad(coords2.latitude - coords1.latitude);  // deg2rad below
        var dLon = self.deg2rad(coords2.longitude - coords1.longitude);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(self.deg2rad(coords1.latitude)) * Math.cos(self.deg2rad(coords2.latitude)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        if (parseFloat(d) < 1 || inMeters) {
            var meters = (parseFloat(d) * 1000).toFixed(0);
            if (!noRounded) {
                if (meters > 300)
                    meters = Math.round(meters / 50) * 50;
                else
                    meters = Math.round(meters / 10) * 10;
            }
            return meters + 'm'; // Return distance in m with format X.X
        }
        else
            return parseFloat(d).toFixed(1) + 'km'; // Return distance in km with format X.X
    };
    /**
     * Converts degrees to the radian equivalent
     * @param deg <number>: Degrees to convert
     * @returns <number>: The equivalent radian for degrees
     */
    self.deg2rad = function (deg) {
        return deg * (Math.PI / 180);
    };

    /**
     * Return the request to Google Geocoding of given LatLng Google Maps Object or current GPS position
     * @param coords <LatLng Google Maps>
     */
    self.geocoding = function (coords) {
        var coordinates = {
            lat: $rootScope.gps.latitude,
            lng: $rootScope.gps.longitude
        };
        if (coords.toJSON !== 'undefined') {
            coordinates = coords.toJSON();
        }
        return $http.get('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAEmSkqgDu-GafUthP0zN8wON3JdS5AIvM&latlng=' + coordinates.lat + ',' + coordinates.lng)
            .then(function (response) {
                return response.data.results[0];
            });
    };

    return self;

}]);
