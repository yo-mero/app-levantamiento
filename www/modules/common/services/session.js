var app = angular.module('app.common.service.session', []);

app.service('Session', ['$rootScope', '$q','DBService', function($rootScope, $q, DBService) {
    var selfObjArray = [];
    var self = {};
    var factoryName = 'Session';

    function pad(s) {
        return (s < 10) ? '0' + s : s;
    }

    self.init = function () {
        // Read from DB
        var deferPromise = $q.defer();
        DBService.DB_action([], factoryName, "GETALL").then(function (result) {
            if (result.rows.length > 0) {
                selfObjArray[0] = result.rows.item((result.rows.length - 1));
                deferPromise.resolve();
            }
            else {
                var newObj = {
                    id: 1,
                    username: "",
                    access_token: "",
                    token_type: "",
                    refresh_token: "",
                    row_version: 0
                };

                selfObjArray.push(newObj);
                DBService.DB_action(newObj, factoryName, "INSERT").then(function () {
                    deferPromise.resolve();
                });
            }
        });
        return deferPromise.promise;
    };

    self.getAll = function () {
        // Get all
        return selfObjArray;
    };
    self.setAll = function (newObjArray) {
        // Set one by one
        for (var i = 0; i < newObjArray.length; i++) {
            self.setSingle(newObjArray[i]);
        }
    };
    self.clear = function () {
        // Erase all
        selfObjArray = [];
        return DBService.DB_action(newObj, factoryName, "DELETEALL");
    };
    self.getSingle = function (id) {
        // Get by id
        for (var i = 0; i < selfObjArray.length; i++) {
            if (selfObjArray[i].id == id) {
                return selfObjArray[i];
            }
        }
        return null;
    };
    self.setSingle = function (newObj) {
        // Set by id
        var requiredId = self.getSingle(newObj.id);
        if (requiredId !== null) {
            // Found, update it
            selfObjArray[requiredId] = newObj;
            return DBService.DB_action(newObj, factoryName, "UPDATE");
        }
        else {
            // Not found, insert it
            selfObjArray.push(newObj);
            return DBService.DB_action(newObj, factoryName, "INSERT");
        }
    };

    // SESSION SPECIFIC FUNCTIONS
    self.getRowVersion = function () {
        if (self.getSingle(1) !== null) {
            return self.getSingle(1).row_version;
        }
        return 0;
    };
    self.setRowVersion = function (rowVersion) {
        if (new Date(rowVersion).getTime() > new Date(selfObjArray[0].row_version).getTime()) {
            console.debug("Actualizando row version a " + rowVersion);
            selfObjArray[0].row_version = rowVersion;
            return DBService.DB_action(selfObjArray[0], factoryName, "UPDATE");
        }
    };

    return self;

}]);