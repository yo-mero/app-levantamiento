angular.module('app.common.service.loadingscreen', [])
    .service('loadingScreen', ['$compile', '$rootScope',
        function ($compile, $rootScope) {
            var self = {};

            self.element = angular.element('<div id="loadingScreen"><ion-spinner></ion-spinner></div>');
            $compile(self.element)($rootScope.$new());

            self.show = function () {
                angular.element(document).find('body').append(self.element);
            };
            self.hide = function () {
                angular.element(self.element).remove();
            };

            return self;
        }
    ]);