/**
 * Created by gcuellar on 8/11/17.
 */
angular.module('app.common.service.liftingsync', [])
    .service('liftingSync', ['$q', '$ionicPopup', 'DBService', 'RestService', 'Session',
        function ($q, $ionicPopup, DBService, RestService, Session) {
            var self = {};

            self.sync = function () {
                var deferPromise = $q.defer();
                // Sync from server
                deferPromise.reject('test');

                return deferPromise.promise;
            };

            return self;
        }
    ]);