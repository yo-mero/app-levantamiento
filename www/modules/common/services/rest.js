angular.module('app.common.rest', []).service('RestService',
['$rootScope', '$state', '$q', '$http', '$log', 'REST_ENDPOINT', 'REST_TIMEOUT_DEFAULT', 'REST_RETRY_DEFAULT', 'REST_OAUTH_SECRET', 'ENVIRONMENT',
    function($rootScope, $state, $q, $http, $log, REST_ENDPOINT, REST_TIMEOUT_DEFAULT, REST_RETRY_DEFAULT, REST_OAUTH_SECRET, ENVIRONMENT) {
        // aqui se definen todos los request que se pueden hacer en el servidor,
        // se define un objeto en formato json, y a cada objeto con un subobjeto que define el request
        // uri que es el path de la url, type tipo de metodo, content, y si es asyncrono o no.
        var configurations = {
            "LiftingGET": {
                "uri": "lifting",
                "type": "GET",
                "content": "application/json",
                "timeout": REST_TIMEOUT_DEFAULT,
                "retry": REST_RETRY_DEFAULT,
                "async": false
            },
            "LiftingPOST": {
                "uri": "lifting",
                "type": "POST",
                "content": "application/x-www-form-urlencoded",
                "timeout": REST_TIMEOUT_DEFAULT,
                "retry": REST_RETRY_DEFAULT,
                "async": false
            },
            "ConflictPOST": {
                "uri": "conflict",
                "type": "POST",
                "content": "application/x-www-form-urlencoded",
                "timeout": REST_TIMEOUT_DEFAULT,
                "retry": REST_RETRY_DEFAULT,
                "async": false
            }
        };

        var request = {};

        var envParam = '';
        switch (ENVIRONMENT) {
            case 'P':
                break;
            case 'D':
                envParam = '&env=demo';
                break;
            case 'X':
                envParam = '&env=develop';
                break;
            default:
                $log.warn('Wrong environment selected defaulting to develop');
                envParam = '&env=develop';
                break;
        }

        //API SERVICES
        request.sendRequest = function ($params, $code, $uriParams) {
            //aqui se retorna el contenido del http como objeto
            var deferPromise = $q.defer();
            return $http({
                method: configurations[$code].type,
                url: REST_ENDPOINT + configurations[$code].uri + "?apiToken=" + REST_OAUTH_SECRET + "&" + $uriParams + envParam,
                timeout: configurations[$code].timeout,
                data: $params,
                headers: {
                    'Content-Type': configurations[$code].content
                    //'Authorization' : $rootScope.oauth.token_type + " " + $rootScope.oauth.access_token
                }
            })
                    .then(function successCallback(response) {
                        $rootScope.rest.retryCnt = 0;
                        deferPromise.resolve(response.data);
                        return response.data;
                    })
                    .catch(function errorCallback(response) {
                        var errorResponse = request.buildErrorResponse(response.status, response.data);

                        return request.processError($http, response.status, response.data, configurations[$code].retry)
                                .then(function success(retry) {
                                    if (retry === true) {
                                        //Retrying
                                        return (request.sendRequest($params, $code, $uriParams));
                                    }
                                }, function error() {
                                    //Not Retrying
                                    deferPromise.reject(errorResponse);
                                    return deferPromise.promise;
                                });
                    });
        };

        // @return true: retry, false: don't retry
        request.processError = function ($http, $responseCode, $response, maxRetry) {
            //console.debug("HTTP Error: " + $responseCode + " retryCounter: " + $rootScope.rest.retryCnt + "/" + maxRetry + " response: " + angular.toJson($response));
            $rootScope.rest.retryCnt++;
            var deferPromise = $q.defer();

            switch ($responseCode) {
                case 400:
                case 401:
                    if ($response.error == "invalid_token") {
                        if ($rootScope.rest.retryCnt < maxRetry) {
                            //Renovar token
                            return false;
                        } else {
                            //Maximo numero de intentos alcanzados
                            deferPromise.reject(false);
                        }
                    } else {
                        deferPromise.reject(false);
                        return deferPromise.promise;
                    }
                    break;

                case 403:
                    //Forbidden
                    deferPromise.reject(false);
                    return deferPromise.promise;
                    break;

                case -1:
                    //Timeout
                    break;

                case 500:
                    deferPromise.reject(false);
                    return deferPromise.promise;
                    break;

                default:
                    break;

            }

            if ($rootScope.rest.retryCnt < maxRetry) {
                //console.debug("Reintento " + $rootScope.rest.retryCnt + " de " + maxRetry);
                deferPromise.resolve(true);
                return deferPromise.promise;
            } else {
                // Max retries exceeded
                $rootScope.rest.retryCnt = 0;
                deferPromise.reject(false);
                return deferPromise.promise;
            }
        };

        request.buildErrorResponse = function (status, data) {
            var errorResponse = {};
            var defaultMessage = "Su solicitud no pudo ser procesada, por favor intente mas tarde";

            switch (true) {
                case (status <= 0):
                case (status == 408):
                    errorResponse.code = "ERR000";
                    errorResponse.message = "No hay conexión con el servidor, revise su conexión a internet e intente nuevamente";
                    break;

                case (status == 400):
                case (status == 401):
                    //console.debug(data.error);
                    if (data.error != "invalid_token") {
                        errorResponse.code = "ERR400a";
                        errorResponse.message = "Usuario o contraseña incorrecto";
                    }
                    else {
                        errorResponse.code = "ERR401";
                        errorResponse.message = "Su sesión expiró, favor de ingresar nuevamente";
                    }
                    break;

                case (status == 403):
                    errorResponse.code = "ERR403";
                    errorResponse.message = "No tiene autorización para ejecutar esta función";
                    break;

                case (status == 409):
                    errorResponse.code = "ERR409";
                    errorResponse.message = ( (!!data.message) ? data.message : "");
                    break;

                case (status == 500):
                    errorResponse.code = ( ((data.code !== undefined) && (data.code !== null)) ? data.code : "500");
                    errorResponse.message = ( ((data.message !== undefined) && (data.message !== null)) ? data.message : defaultMessage);
                    break;

                default:
                    errorResponse.code = "ERR00F";
                    errorResponse.message = defaultMessage;
                    break;
            }
            return errorResponse;
        };

        return request;

    }
]);