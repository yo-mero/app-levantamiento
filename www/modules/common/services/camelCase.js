/**
 * Created by gcuellar on 10/11/17.
 */
angular.module('app.common.service.camelcase', [])
    .service('camelCase', [function () {
            var self = {};

            self.fromString = function (string, separator) {
                separator = separator || '_';
                string.replace(new RegExp(separator + "([a-z])","g"), function (g) { return g[1].toUpperCase(); })
                return string;
            };
            self.toSeparate = function (camelCaseString, separator) {
                separator = separator || '_';
                return camelCaseString.replace(/([a-z0-9])([A-Z])/g, '$1' + separator + '$2').toLowerCase();
            };

            return self;
        }
    ]);