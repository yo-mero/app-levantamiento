angular.module('app.common.service.issues', [])
    .service('Issues', ['$rootScope', '$q', 'DBService', 'RestService', function($rootScope, $q, DBService, RestService) {
        var selfObjArray = [];
        var self = {};
        var factoryName = 'Issues';
        self.promises = [];

        self.init = function () {
            var deferPromise = $q.defer();

            DBService.DB_action([], factoryName, "GETALL").then(function (result) {
                selfObjArray = DBService.getResult(result);
                var timestamp = 0;
                var indexGreatRowVersion = -1;
                for (var i = 0, rv = 0; i < selfObjArray.length; i++) {
                    if (Date.parse(selfObjArray[i].row_version) > rv) {
                        rv = Date.parse(selfObjArray[i].row_version);
                        indexGreatRowVersion = i;
                    }
                }
                if (indexGreatRowVersion >= 0)
                    timestamp = Date.parse(selfObjArray[indexGreatRowVersion].row_version) / 1000;

                var lastIDRegister = parseInt( Math.max.apply(Math, selfObjArray.map(function (obj) { return obj.id; })) );
                if (isNaN(lastIDRegister) || lastIDRegister < 0)
                    lastIDRegister = 0;
                RestService.sendRequest(null, 'IssuesGET', 'action=all&row_version=' + timestamp + '&local_numrows=' + selfObjArray.length + '&lastID=' + lastIDRegister).then(function (response) {
                    if (!!response.length) {
                        //self.clear();
                        self.setAll(response).then(function () {
                            deferPromise.resolve();
                        }, function () {
                            deferPromise.reject();
                        });
                    }
                    else
                        deferPromise.resolve();
                }, function () {
                    deferPromise.reject();
                });
            });

            return deferPromise.promise;
        };
        self.getAll = function () {
            // Get all
            return selfObjArray;
        };
        self.setAll = function (newObjArray) {
            var deferPromise = $q.defer();
            self.promises = [];
            // Set one by one
            for (var i = 0; i < newObjArray.length; i++) {
                self.promises.push(self.setSingle(newObjArray[i]));
            }

            $q.all(self.promises).then(function () {
                deferPromise.resolve();
            }, function (error) {
                deferPromise.reject(error);
            });

            return deferPromise.promise;
        };
        self.getSingle = function(id){
            // Get by id
            for (var i = 0; i < selfObjArray.length; i++){
                if (selfObjArray[i].id == id){
                    return selfObjArray[i];
                }
            }
            return null;
        };
        self.setSingle = function (newObj) {
            var deferPromise = $q.defer();

            var requiredId = self.getSingle(newObj.id);
            if (requiredId !== null) {
                // Found, update it
                selfObjArray[requiredId] = newObj;
                DBService.DB_action(newObj, factoryName, "UPDATE").then(function () {
                    deferPromise.resolve();
                }, function () {
                    deferPromise.reject();
                });
            }
            else {
                // Not found, insert it
                selfObjArray.push(newObj);
                DBService.DB_action(newObj, factoryName, "INSERT").then(function () {
                    deferPromise.resolve();
                }, function () {
                    deferPromise.reject();
                });
            }

            return deferPromise.promise;
        };
        self.clear = function () {
            // Erase all
            selfObjArray = [];
            DBService.DB_action([], factoryName, "DELETEALL");
        };

        return self;
    }]);