angular.module('app.common.service.device', [])
    .service('Device', ['$cordovaDevice',
        function ($cordovaDevice) {
            return {
                Info: function () {
                    return window.cordova ? $cordovaDevice.getDevice() : '';
                },
                InfoForRequest: function () {
                    var device = this.Info();
                    return window.cordova ? 'device=' + device.manufacturer + ' ' + device.model + ' ' + device.serial + '&uuid=' + device.uuid : '';
                }
            };
        }
    ]);