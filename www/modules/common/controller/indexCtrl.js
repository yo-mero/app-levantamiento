angular.module('app').controller('IndexController',
['$scope', '$state', 'loadingScreen', 'liftingSync', '$ionicPlatform',
    function ($scope, $state, loadingScreen, liftingSync, $ionicPlatform) {
        $scope.goTo = function (module) {
            loadingScreen.show();
            switch (module) {
                case 'lifting':
                    liftingSync.sync().finally(function () {
                        loadingScreen.hide();
                        $state.go('app.lifting');
                    });
                    break;
                default:
                    loadingScreen.hide();
                    break;
            }
        };
        $ionicPlatform.ready(function () {
            $scope.goTo('lifting');
        });
    }
]);
